1. 将数据库模式由非归档改为归档模式
2. 查看v$log 和 v$logfile;
3. 增加一个日志文件组4，组中放入一个成员
4. 向日志文件组4 再增加一个成员
5. 将 一个日志文件 移动到新地方
    a. 关闭数据库
    b. 移动，重命名日志文件
    c. 启动 mount
    d. alter dtabase rename file '' to ''
    e. 打开数据库
6. 删除日志成员 和日志文件组（current、active）
    alter system switch logfile;
    alter system checkpoint
    检查一下对于物理文件是否被删除
7. OMF
    检查一下对于物理文件是否被删除
8. 设置归档目的地并测试归档
9. 日志文件的修复
    非当前日志文件损坏
        alter database clear logfile group n;
        alter database clear logfile unarchived logfile group n;
    当前日志文件损坏
        sql>recover database until cancel;
            alter database open resetlogs;
            archive log list;

