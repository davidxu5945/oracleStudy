# 快速恢复数据

##闪回
1. fast
2. easy
3. flash back any error (逻辑错误) 解决不了物理错误

###闪回数据库

前提：
1. 启用归档模式
2. 启用闪回日志 
    select flashback_on from v$database;
    alter database flashback on;
3. 和闪回相关的初始化参数
    show parameter recover
    db_recovery_file_dest       --- 闪回日志的存放位置
    db_recovery_file_dest_size  --- 最大闪回日志大小
    db_flashback_retention_target --- 闪回日志的保存时间 默认一天 1440min
    
    desc v$flashback_database_log
    OLDEST_FLASHBACK_SCN --最旧的闪回时间 此时间之前的

    select current_scn from v$database;
    drop user scott cascade; --删除用户和其下的对象
    drop table hr.emp cascade constraint；--连同主外键一起删除
    shutdown immediate
    startup mount;
    flashback database to scn 570734; 
    alter database open read only; --只读方式打开数据，查看是否闪回正确
    -- 确定数据无误，重新关闭，启动到mount
    alter database open resetlogs;

### 闪回表
不能闪回系统表 （所有者是sys，在system表空间）
前提：
1. 激活表的行移动特性 
    desc user_tables
    select table_name,row_movement from user_tables;
    alter table emp enable row movement;
    $time --当前时间
    flashback table emp to timestamp(to_timestamp('2015-01-29 18:43 10'))

### 闪回删除
前提
1. 启用回收站

show parameter recyclebin -- 默认开启
flashback table emp to before drop;
show recyclebin

### 闪回查询
####闪回查询表
select × from emp as of timestamp(to_timestamp('2015-01-29 18:43:10','yyyy-mm-dd hh24:mi:ss')) where deptno=30;
####闪回版本查询
select sal from emp versions between scn minvalue and maxvalue 
where empno=1234;
####闪回事务查询
前提
1.启用数据库补充日志
    flashback_transanc
    set page size 20  -- 设置查询每页多少数据

    select empno,ename,versions_xid,versions_operation from my_emp 
    versions between scn minvalue and maxvalue;
    desc flashback_transaction_query;
    select UNDO_SQL from flashback_transaction_query from 
####闪回事务 11g
dbms_flashback.transaction_backout
WAW依赖
主键依赖
外键依赖 
####闪回数据归档 11g
创建闪回归档区
启动归档进程


AWR
@?\rdbms\admin\awrrpt
html
3天内的快照

ADDM


RBO
CBO
select /*+ rule /* from test3 where n1=100;

show parameter mult
db_file_multiblock_read_count       --全表扫描 一次性读取的block数
show parameter adj          100   权重 索引 全表