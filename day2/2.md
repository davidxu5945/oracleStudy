*   backup
*   restore 
*   recover

监听进程
在命令提示符下输入
    lsnrctl stop --停止监听
    lsnrctl status -- 查看监听状态
    set nls_lang=english 设置环境变量，sqlplus中的输出不是问号

调度进程
请求队列
响应队列

专用服务器进程（客户连接在200以下）
共享服务器进程（客户连接在200以上，2000以下）
中间件（2000以上） weblogic websphere

connect 连接（找到服务器进程）
session 会话（开始被调度）


PMON 进程监视进程 PROCESS MONITOR
SMON 系统监视进程 SYSTEM MONITOR 
DBWR 数据写入进程 脏数据，灰数据，写入数据文件（后记）
LGWR 日志写入进程（先记）
CKPT 检查点进程  CHECK POINT 